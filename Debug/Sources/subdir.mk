################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
ASM_SRCS += \
../Sources/DSP56364_VECS.asm \
../Sources/main.asm 

OBJS += \
./Sources/DSP56364_VECS.cln \
./Sources/main.cln 


# Each subdirectory must supply rules for building sources it contributes
Sources/%.cln: ../Sources/%.asm
	@echo 'Building file: $<'
	@echo 'Invoking: 56K ASM Assembler'
	asm56300 -G -L$(basename $(<F)).lst -B"$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


