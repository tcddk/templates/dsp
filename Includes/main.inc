; This is for conditional assembly some parts of the code to help development
SIM				EQU		0		; This is for use the simulator
TEST			EQU		1		; This for debug using the pedal and the GUI
RT				EQU		2		; This for the final release of the effect
ASM_TYPE		EQU		TEST

Fs				EQU		39062.5		; This is the sample rate of the signal
