		PAGE		80,100,1,1,1
		TABS		3
		OPT			MEX
		
		SECTION		VECTORS
		
		GLOBAL		VECS_START
		
		XREF		START
		
		XREF		SHI_RX_ISR
		XREF		ESAI_RX_EX_ISR
		XREF		ESAI_RX_ISR
		XREF		ESAI_RX_EVEN_ISR
		
		ORG			P:$000000
		
VECS_START

		JMP			START				;VBA:$00 3 Hardware RESET
		;NOP
		
		JMP			*					;VBA:$02 3 Stack Error
		NOP
		
		JMP			*					;VBA:$04 3 Illegal Instruction
		NOP
		
		JMP			*					;VBA:$06 3 Debug Request Interrupt
		NOP
		
		JMP			*					;VBA:$08 3 Trap
		NOP
		
		JMP			*					;VBA:$0A 3 Non-Maskable Interrupt (NMI)
		NOP
		
		NOP								;VBA:$0C 3 Reserved For Future Level-3 Interrupt Source
		NOP
		
		NOP								;VBA:$0E 3 Reserved For Future Level-3 Interrupt Source
		NOP
		
		JMP			*					;VBA:$10 0 - 2 IRQA
		NOP
		
		JMP			*					;VBA:$12 0 - 2 IRQB
		NOP
		
		NOP								;VBA:$14 0 - 2 Reserved
		NOP
		
		JMP			*					;VBA:$16 0 - 2 IRQD
		NOP
		
		JMP			*					;VBA:$18 0 - 2 DMA Channel 0
		NOP
		
		JMP			*					;VBA:$1A 0 - 2 DMA Channel 1
		NOP
		
		JMP			*					;VBA:$1C 0 - 2 DMA Channel 2
		NOP
		
		JMP			*					;VBA:$1E 0 - 2 DMA Channel 3
		NOP
		
		JMP			*					;VBA:$20 0 - 2 DMA Channel 4
		NOP
		
		JMP			*					;VBA:$22 0 - 2 DMA Channel 5
		NOP
		
		NOP								;VBA:$24 0 - 2 Reserved
		NOP
		
		NOP								;VBA:$26 0 - 2 Reserved
		NOP
		
		NOP								;VBA:$28 0 - 2 Reserved
		NOP
		
		NOP								;VBA:$2A 0 - 2 Reserved
		NOP
		
		NOP								;VBA:$2C 0 - 2 Reserved
		NOP
		
		NOP								;VBA:$2E 0 - 2 Reserved
		NOP
		
		JSR			ESAI_RX_ISR			;VBA:$30 0 - 2 ESAI Receive Data
		;NOP
		
		JSR			ESAI_RX_EVEN_ISR	;VBA:$32 0 - 2 ESAI Receive Even Data
		;NOP
		
		JSR			ESAI_RX_EX_ISR		;VBA:$34 0 - 2 ESAI Receive Data With Exception Status 
		;NOP
		
		JMP			*					;VBA:$36 0 - 2 ESAI Receive Last Slot 
		NOP
		
		JMP			*					;VBA:$38 0 - 2 ESAI Transmit Data
		NOP
		
		JMP			*					;VBA:$3A 0 - 2 ESAI Transmit Even Data
		NOP
		
		JMP			*					;VBA:$3C 0 - 2 ESAI Transmit Data with Exception Status
		NOP
		
		JMP			*					;VBA:$3E 0 - 2 ESAI Transmit Last Slot 
		NOP
		
		JMP			*					;VBA:$40 0 - 2 SHI Transmit Data
		NOP
		
		JMP			*					;VBA:$42 0 - 2 SHI Transmit Underrun Error
		NOP
		
		JSR			SHI_RX_ISR			;VBA:$44 0 - 2 SHI Receive FIFO Not Empty
		;NOP
		
		NOP								;VBA:$46 0 - 2 Reserved
		NOP
		
		JMP			*					;VBA:$48 0 - 2 SHI Receive FIFO Full
		NOP
		
		JMP			*					;VBA:$4A 0 - 2 SHI Receive Overrun Error
		NOP
		
		JMP			*					;VBA:$4C 0 - 2 SHI Bus Error
		NOP
		
		ENDSEC
		