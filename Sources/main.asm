		PAGE		80,100,1,1,1
		TABS		3
		OPT			MEX,CEX
		
		SECTION		PROJECT
		
		NOLIST
		
		INCLUDE		'..\Includes\DSP56364_EQU.inc'
		INCLUDE		'..\Includes\DSP56364_CFG.inc'
		INCLUDE		'..\Includes\main.inc'
		
		LIST
		
		SECTION		SHARED_DATA		GLOBAL
		
		ORG			X:$000000
		
Knob_1						DC		$400000		; 0x000000
Knob_2						DC		$400000		; 0x000001
Knob_3						DC		$400000		; 0x000002
Knob_4						DC		$400000		; 0x000003
Knob_5						DC		$400000		; 0x000004
Knob_6						DC		$400000		; 0x000005

Switch_1					DC		$000000		; 0x000006
Switch_2					DC		$000000		; 0x000007

FootSwitchTap				DC		$000000		; 0x000008
FootSwitch					DC		$000000		; 0x000009

LED_Red						DC		$000000		; 0x00000A
LED_Red_PWM					DC		$000000		; 0x00000B
LED_Green					DC		$000000		; 0x00000C
LED_Green_PWM				DC		$000000		; 0x00000D

Debug_Write_to_DSP_1		DC		$000000		; 0x00000E
Debug_Write_to_DSP_2		DC		$000000		; 0x00000F
Debug_Write_to_DSP_3		DC		$000000		; 0x000010
Debug_Write_to_DSP_4		DC		$000000		; 0x000011
Debug_Read_from_DSP_1		DC		$000000		; 0x000012
Debug_Read_from_DSP_2		DC		$000000		; 0x000013
Debug_Read_from_DSP_3		DC		$000000		; 0x000014
Debug_Read_from_DSP_4		DC		$000000		; 0x000015

LeftRx						DC		$000000
RightRx						DC		$000000
LeftTx						DC		$000000
RightTx						DC		$000000
SHI_StateMachine			DC		$000000
HostCommand					DC		$000000

		ENDSEC		; SHARED_DATA
		

		SECTION		MAIN_VARS		GLOBAL
		
		ORG			Y:
		
LeftInput				DC		$000000
LeftOutput				DC		$000000
RightInput				DC		$000000
RightOutput				DC		$000000

FootSwitchTapMem		DC		0
FootSwitchTapEdge		DC		0
FootSwitchMem			DC		0
FootSwitchEdge			DC		0
FxState					DC		0

LED_MAX_PWM				DC		50000
EXTERNAL_RAM_SIZE		DC		174762
TEST_VARIABLE			DC		0
		
		ENDSEC		; MAIN_VARS
		
		
		SECTION		MAIN
		
		XDEF		START
		
		ORG			P:
		
START
		
		MOVEC		#SR_CFG,SR
		MOVEC		#OMR_CFG,OMR
		
		MOVEP		#IPRC_CFG,X:IPRC
		MOVEP		#IPRP_CFG,X:IPRP
		
		MOVEP		#PCTL_CFG,X:PCTL
		
		MOVEP		#HCKR_CFG,X:HCKR
		MOVEP		#HCSR_CFG,X:HCSR
		MOVE		#$00FFFF,M7
		
		MOVEP		#TCCR_CFG,X:TCCR
		MOVEP		#TCR_CFG,X:TCR
		MOVEP		#RCCR_CFG,X:RCCR
		MOVEP		#RCR_CFG,X:RCR
		MOVEP		#SAICR_CFG,X:SAICR
		MOVEP		#$000003,X:TSMA
		MOVEP		#$000000,X:TSMB
		MOVEP		#$000003,X:RSMA
		MOVEP		#$000000,X:RSMB
		
		MOVEP		#$000000,X:PDRC
		MOVEP		#$000BF8,X:PCRC
		MOVEP		#$000C7E,X:PRRC
		
		MOVEP		#$000000,X:TX0
		BSET		#RCR_RE0,X:RCR
		BSET		#TCR_TE0,X:TCR
		
		MOVEP		#$000000,X:PDRB
		MOVEP		#$000000,X:PCRB
		MOVEP		#$00000F,X:PRRB
		
		MOVEP		#BCR_CFG,X:BCR
		MOVEP		#AAR0_CFG,X:AAR0
		MOVEP		#AAR1_CFG,X:AAR1
		
FILL_EXTERNAL_RAM
		
		CLR			A
		MOVEP		#TEST_VARIABLE,X:DSR1
		MOVEP		#$080000,X:DDR1
		MOVEP		#>0,X:DCO1
		MOVEP		#>3,X:DOR0
		JCLR		#DSTR_DTD1,X:DSTR,*
		MOVE		A,Y:TEST_VARIABLE
		DO			Y:EXTERNAL_RAM_SIZE,ENABLE_INTERRUPTS
		MOVEP		#$982841,X:DCR1
		NOP
		NOP
		NOP
		JCLR		#DSTR_DTD1,X:DSTR,*
		MOVE		Y:TEST_VARIABLE,A
		ADD			#1,A
		MOVE		A,Y:TEST_VARIABLE
		
ENABLE_INTERRUPTS
		
		ANDI		#$FC,MR
		MOVEP		#$000000,X:HTX
		MOVEP		#$000002,X:PDRC
		
MAIN_LOOP
		
		IF	ASM_TYPE==RT
		
		WAIT
		JMP			MAIN_LOOP
		
		ENDIF
		
		IF ASM_TYPE==SIM
		
		; Code to use with the simulator
		
FINISH
		
		STOP
		
		ENDIF
		
		IF ASM_TYPE==TEST
		
		; Code to help debugging with the pedal and the GUI
		
		; Debug write to DSP 1: address to read on X memory space
		; Debug write to DSP 2: address to read on Y memory space
		; Debug write to DSP 3: address to read on external RAM [0x080000 - 0x0FFFFB]
		; Debug write to DSP 4: address to read on P memory space
		
		; Debug read from DSP 1: value at address in Debug write to DSP 1 (X memory space)
		; Debug read from DSP 2: value at address in Debug write to DSP 2 (Y memory space)
		; Debug read from DSP 3: value at address in Debug write to DSP 3 (external RAM)
		; Debug read from DSP 4: value at address in Debug write to DSP 4 (P memory space)
		
		; Remember that the external RAM word is 8 bits wide, and that the DSP DMA controller 
		; access tree consecutive bytes to form a 24 bit word, so, the address specified in 
		; Debug write to DSP 3 should be an integer multiple of 3 plus the base address (0x080000)
		; For example: 0x080000, 0x080003, 0x080006, ..., 0x0FFFF5, 0x0FFFF8, 0x0FFFFB
		
		WAIT
		
		MOVE		X:Debug_Write_to_DSP_1,R0
		MOVE		X:(R0),X0
		MOVE		X0,X:Debug_Read_from_DSP_1
		
		MOVE		X:Debug_Write_to_DSP_2,R0
		MOVE		Y:(R0),X0
		MOVE		X0,X:Debug_Read_from_DSP_2
		
		MOVE		X:Debug_Write_to_DSP_3,R0
		MOVEP		R0,X:DSR0
		MOVEP		#Debug_Read_from_DSP_3,X:DDR0
		MOVEP		#>0,X:DCO0
		JCLR		#DSTR_DTD0,X:DSTR,*
		MOVEP		#$982A40,X:DCR0
		NOP
		NOP
		NOP
		JCLR		#DSTR_DTD0,X:DSTR,*
		
		MOVE		X:Debug_Write_to_DSP_4,R0
		MOVEM		P:(R0),X0
		MOVE		X0,X:Debug_Read_from_DSP_4
		
		JMP			MAIN_LOOP
		
		ENDIF
		
		
		ENDSEC		; MAIN
		
		
		SECTION		ISR
		
		XDEF		SHI_RX_ISR
		XDEF		ESAI_RX_EX_ISR
		XDEF		ESAI_RX_ISR
		XDEF		ESAI_RX_EVEN_ISR
		
		ORG			P:
		
SHI_RX_ISR
		
        MOVEP		X:HRX,X:HostCommand
        MOVEP		#000000,X:HTX
        BTST		#0,X:HostCommand
        BCS			DoWriteCommand
        BTST		#3,X:HostCommand
        BCS			DoReadCommand
		
DoWriteCommand
		
        JCLR		#HCSR_HRNE,X:HCSR,*
        MOVEP		X:HRX,R7
        MOVEP		#>000000,X:HTX
		
        JCLR		#HCSR_HRNE,X:HCSR,* 
        MOVEP		X:HRX,X:(R7)
        MOVEP		#000000,X:HTX
		RTI
		
DoReadCommand
		
        JCLR		#HCSR_HRNE,X:HCSR,*
        MOVEP		X:HRX,R7
        MOVEP		#>000000,X:HTX
        
        JCLR		#HCSR_HRNE,X:HCSR,*
        MOVEP		X:HRX,N7
        MOVEP		X:(R7),X:<<HTX
        
        JCLR		#HCSR_HRNE,X:HCSR,*
        MOVEP		X:HRX,N7
        MOVEP		#>000000,X:<<HTX
		RTI
		
ESAI_RX_EX_ISR
		
        BCLR		#SAISR_ROE,X:SAISR
        BCLR		#SAISR_TUE,X:SAISR
        MOVEP		X:RX0,X:RightRx 
        MOVEP		X:RightTx,X:TX0
        RTI
        
ESAI_RX_ISR
		
        MOVEP		X:RX0,X:RightRx       
        MOVEP		X:RightTx,X:TX0       
        RTI
        
ESAI_RX_EVEN_ISR
		
        MOVEP		X:RX0,X:LeftRx        
        MOVEP		X:LeftTx,X:TX0
		
        MOVE		Y:LeftOutput,X0 
        MOVE		X0,X:LeftTx
        MOVE		Y:RightOutput,X0 
        MOVE		X0,X:RightTx
		
        MOVE		X:LeftRx,A
        MOVE		X:RightRx,B
		
        MOVE		A,Y:LeftInput
        MOVE		B,Y:RightInput
        
        ; Process signal here
        
        MOVE		A,Y:LeftOutput
        MOVE		B,Y:RightOutput
        
FOOT_SWITCH
		
		; Rising edge detection on foot switch and tap switch
		
		; The variables xxxEdge are one (0x000001) when there is a rising edge 
		; on the respective switch and hold that one (0x000001) for only one sample period
        
        MOVE		Y:FootSwitchTapMem,A
        NOT			A		X:FootSwitchTap,X0
        AND			X0,A
        MOVE		X0,Y:FootSwitchTapMem
        MOVE		A,Y:FootSwitchTapEdge
        
        MOVE		Y:FootSwitchMem,A
        NOT			A		X:FootSwitch,X0
        AND			X0,A
        MOVE		X0,Y:FootSwitchMem
        MOVE		A,Y:FootSwitchEdge
        JCLR		#0,Y:FootSwitchEdge,END_FOOT_SWITCH
        BCHG		#0,Y:FxState
        
END_FOOT_SWITCH
		
		JCLR		#0,Y:FxState,FX_OFF
        
FX_ON
		
		; The MCU code has been modified to allow the leds to be controlled either 
		; binary (on - off) or with PWM to vary their brightness. The PWM value 
		; must be between 0 (brightness: 0%) and 50000 (brightness: 100%)
		
		BSET		#0,X:LED_Green
		BCLR		#0,X:LED_Red
		CLR			A		Y:LED_MAX_PWM,X0
		MOVE		X0,X:LED_Green_PWM
		MOVE		A,X:LED_Red_PWM
		MOVEP		#>$000008,X:PDRB
		RTI
		
FX_OFF
		
		CLR			A
		BCLR		#0,X:LED_Green
		BCLR		#0,X:LED_Red
		MOVE		A,X:LED_Green_PWM
		MOVE		A,X:LED_Red_PWM
		MOVEP		#>$000004,X:PDRB
		RTI
        
        ENDSEC		; ISR
        
        ENDSEC		; PROJECT
		